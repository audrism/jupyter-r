#!/bin/bash
u=$1
uid=$2
gid=$3
groupadd --gid $gid $u 
useradd --uid $uid --gid $gid $u 
sed -i 's/^$/+ : '$u' : ALL/' /etc/security/access.conf
echo "$u ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/$u

sleep 5
sudo -H -u $u sh -c /bin/bash
